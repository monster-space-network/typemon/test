# Test
> Testing with class-based and dependency injection

[![npm-version](https://img.shields.io/npm/v/@typemon/test.svg)](https://www.npmjs.com/package/@typemon/test)
[![npm-downloads](https://img.shields.io/npm/dt/@typemon/test.svg)](https://www.npmjs.com/package/@typemon/test)
